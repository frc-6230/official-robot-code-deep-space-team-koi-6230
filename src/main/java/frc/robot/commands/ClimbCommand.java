/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.IO;

import static frc.robot.Robot.climber;

public class ClimbCommand extends Command {
  public ClimbCommand() {
    requires(climber);
  }

  @Override
  protected void initialize() {
    climber.stop();
  }

  @Override
  protected void execute() {
    
  }

  @Override
  protected boolean isFinished() {
    return false;
  }

  @Override
  protected void end() {
    climber.stop();
  }

  @Override
  protected void interrupted() {
    end();
  }
}
