/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.FlippingState;
import frc.robot.IO;
import frc.robot.RobotMap;

import static frc.robot.Robot.flipper;
import static frc.robot.Robot.elevator;
public class FlipSystemCommand extends Command {

  private PIDController controller;
  private double angle;
  private FlippingState state;
  private boolean auto;



  public FlipSystemCommand(FlippingState state) {
    requires(flipper);
    this.state = state;
    this.angle = state.getAngle();

    
  }

  public void setState(FlippingState state){
    this.state = state;
  }

  @Override
  protected void initialize() {
    flipper.stop();
    controller = new PIDController(0.2,0,0,new PIDSource(){
    
      @Override
      public void setPIDSourceType(PIDSourceType pidSource) {
        
      }
    
      @Override
      public double pidGet() {
        return flipper.getAngle();
      }
    
      @Override
      public PIDSourceType getPIDSourceType() {
        return flipper.getEncoder().getPIDSourceType();
      }
    },flipper.getVictor());
    controller.setAbsoluteTolerance(8);
    controller.setSetpoint(angle);
    controller.setOutputRange(-1, 1);
   // controller.enable();
    auto = true;
  }

  @Override
  protected void execute() {

    // if(controller.onTarget()){
    //   flipper.setState(state);
    //   flipper.setOnTarget(controller.onTarget());
    // }
    if(IO.elevatorJoy.getRawButton(1))
    {
      controller.disable();
      flipper.rotate(IO.getFlippingAxis()*0.8);
    }else{
      //flipper.rotate(0);
      controller.enable();
      
      if(elevator.getHeight() > RobotMap.ROBOT_FLIP_SAFE_ZONE_BOTTOM && elevator.getHeight() < RobotMap.ROBOT_FLIP_SAFE_ZONE_TOP){
        controller.setSetpoint(state.getAngle());
      }
    }
    
      
  }

  @Override
  protected boolean isFinished() {
    return false;
  }

  @Override
  protected void end() {
    controller.disable();
    flipper.stop();

  }

  @Override
  protected void interrupted() {
    end();
  }
}
