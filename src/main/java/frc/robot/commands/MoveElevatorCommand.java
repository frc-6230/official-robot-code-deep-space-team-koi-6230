/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import static frc.robot.Robot.elevator;
import frc.robot.IO;

public class MoveElevatorCommand extends Command {
  public MoveElevatorCommand() {
    requires(elevator);
  }

  @Override
  protected void initialize() {
    elevator.Stop();
  }

  @Override
  protected void execute() {
    if(!elevator.auto){
      elevator.move(IO.getElevatorAxis());
    }
  }

  @Override
  protected boolean isFinished() {
    return false;
  }

  @Override
  protected void end() {
    elevator.Stop();
  }


  @Override
  protected void interrupted() {
  }
}
