/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import static frc.robot.Robot.grip;

public class GripChangeState extends Command {

  private double lastTime;
  private boolean changed;
  public GripChangeState() {
    requires(grip);
    changed = false;
  }

  @Override
  protected void initialize() {
    lastTime = Timer.getFPGATimestamp();
    changed = false;
    grip.setUltra(false);
    grip.changeState();
    
  }

  @Override
  protected void execute() {
    if(Timer.getFPGATimestamp() - lastTime >= 0.5 && !changed){
      changed = true;
      grip.closePush();
    }
  }

  @Override
  protected boolean isFinished() {
    return Timer.getFPGATimestamp() - lastTime >= 1;
  }

  @Override
  protected void end() {
    grip.setUltra(true);
    
  }

  @Override
  protected void interrupted() {
  }
}
