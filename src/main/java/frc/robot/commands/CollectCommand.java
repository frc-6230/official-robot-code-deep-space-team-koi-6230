/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.IO;

import static frc.robot.Robot.collector;

public class CollectCommand extends Command {
  public CollectCommand() {
    requires(collector);
  }

  @Override
  protected void initialize() {
    collector.stop();
  }

  @Override
  protected void execute() {
    collector.collect(IO.getCollectorAxis());
  }                                                 
  

  @Override
  protected boolean isFinished() {
    return false;
  }

  @Override
  protected void end() {
    collector.stop();
  }


  @Override
  protected void interrupted() {
    end();
  }
}
