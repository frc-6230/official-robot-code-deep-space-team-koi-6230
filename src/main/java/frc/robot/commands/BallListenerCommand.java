/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.ElevatorHeights;
import frc.robot.Robot;

import static frc.robot.Robot.grip;

public class BallListenerCommand extends Command {

  private boolean collected;
  
  public BallListenerCommand() {
    requires(grip);
    
  }

  @Override
  protected void initialize() {
    collected = false;
  }

  @Override
  protected void execute() {
    if(grip.ballCaught() && Robot.height == ElevatorHeights.COLLECT_POSITION){
      collected = true;
      grip.close();
    }else if(!grip.ballCaught()){
      collected = false;
    }
  }

  @Override
  protected boolean isFinished() {
    return false;
  }

  @Override
  protected void end() {
  }

  @Override
  protected void interrupted() {
  }
}
