/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import static frc.robot.Robot.driver;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.IO;

public class DriveCommand extends Command {
  public DriveCommand() {
    requires(driver);
  }

  @Override
  protected void initialize() {
    driver.stop();
  }

  @Override
  protected void execute() {
    if(!driver.isAuto()){
      if (IO.driveJoy.getRawButton(3)) {
        driver.drive(0.4,0.4);  
      }
      else{
        driver.drive(IO.getLeftAxis(),IO.getRightAxis());
      }
      
    }
  }

  @Override
  protected boolean isFinished() {
    return false;
  }

  @Override
  protected void end() {
    driver.stop();
  }


  @Override
  protected void interrupted() {
    end();
  }
}
