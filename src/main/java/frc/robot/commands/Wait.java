/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

public class Wait extends Command {
  private double lastTime;
  private double seconds;

  public Wait(double seconds) {
    this.seconds = seconds;
  }

  @Override
  protected void initialize() {
    lastTime = Timer.getFPGATimestamp();
  }

  @Override
  protected void execute() {
  }

  @Override
  protected boolean isFinished() {
    return Timer.getFPGATimestamp() - lastTime >= seconds;
  }

  @Override
  protected void end() {
  }


  @Override
  protected void interrupted() {
  }
}
