/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * Add your docs here.
 */
public enum FlippingState{
    Closed(90),
    Default(0),
    Collect(-45),
    Throw(135),
    Place(180);

    private double angle;

    private FlippingState(double angle){
        this.angle = angle;
    }

    public double getAngle(){
        return angle;
    }
}
