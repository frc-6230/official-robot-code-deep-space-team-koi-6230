/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.auto;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.IO;

import static frc.robot.Robot.driver;

public class TurnCommand extends Command {

  private double angle;
  private int pos = 0;
  private PIDController controller;
  private double lastTimeOnTarget;
  private double last;
  private boolean stopped;

  public TurnCommand(int pos) {
    requires(driver);

    this.pos = pos;

       
    lastTimeOnTarget = 0;
    
  }


  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    driver.setAuto(true); 
    driver.resetGyro();
    stopped = false;
    try{
      angle = Math.toDegrees(Math.atan((34.5 + (Math.tan(Math.toRadians(SmartDashboard.getEntry("IP_Angle"+pos).getDouble(0))))*SmartDashboard.getEntry("Distance"+pos).getDouble(0))/SmartDashboard.getEntry("Distance" +pos).getDouble(0)));
    }catch(Exception e){
      angle = 0;
    
    }
    last = Timer.getFPGATimestamp();
    SmartDashboard.getEntry("IP_Angle"+pos).setDouble(0);
    SmartDashboard.getEntry("Distance"+pos).setDouble(0);
    System.out.println("angle = " + angle);
    controller = new PIDController(0.1, 0, 0.21, driver.getGyro(), new PIDOutput(){
    
      @Override
      public void pidWrite(double output) {
        driver.drive(-output,output);
      }
    });

    controller.setAbsoluteTolerance(0.5);
    controller.setSetpoint(angle);
    controller.setOutputRange(-0.4, 0.4);
    
    controller.enable();
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    //System.out.println("turning.....");    

  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
   

    if(!controller.onTarget()){
      lastTimeOnTarget = Timer.getFPGATimestamp();
    }

    
    // double update = SmartDashboard.getEntry("IP_Angle").getDouble(0);

    // if(Timer.getFPGATimestamp() - lastTimeOnTarget >= 0.5){
    //   if(auto && Math.abs(update) > 1.5){
    //     angle = update;
    //     controller.setSetpoint(angle);
    //   }else return true;
    // }

    return (Timer.getFPGATimestamp() - last >= 2)||(Timer.getFPGATimestamp() - lastTimeOnTarget >= 0.2) || IO.isAutoDisabled();
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    driver.setAuto(false); 
    System.out.println("finished");
    System.out.println(driver.getGyro().getAngle());
    controller.disable();
    driver.resetGyro();
    driver.stop();
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
    end();
  }
}
