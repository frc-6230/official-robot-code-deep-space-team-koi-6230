/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.auto;
import static frc.robot.Robot.elevator;
import static frc.robot.Robot.flipper;
import static frc.robot.Robot.grip;



import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.ElevatorHeights;
import frc.robot.ElevatorState;
import frc.robot.FlippingState;
import frc.robot.IO;
import frc.robot.Robot;
import frc.robot.RobotMap;


public class AutoElevator extends Command {

  private FlippingState flippingState;
  private ElevatorState elevatorState;
  private ElevatorHeights height;
  private PIDController controller;

  public AutoElevator(ElevatorHeights height) {
    this.elevatorState = height.getElevatorState();
    this.flippingState = height.getFlippingState();
    this.height = height;
  }

  @Override
  protected void initialize() {
    elevator.auto = true;
    elevator.disable = false;
    elevator.Stop();
    if(height == ElevatorHeights.COLLECT_POSITION){
          grip.close();
    }
    controller = new PIDController(0.003,0,0.001, new PIDSource(){
    
      @Override
      public void setPIDSourceType(PIDSourceType pidSource) {
        
      }
    
      @Override
      public double pidGet() {
        return elevator.getencoder().get();
      }
    
      @Override
      public PIDSourceType getPIDSourceType() {
        return elevator.getencoder().getPIDSourceType();
      }
    }, new PIDOutput(){
    
      @Override
      public void pidWrite(double output) {
        elevator.move(output);
      }
    });
    Robot.compressor.setClosedLoopControl(false);
    
    controller.setAbsoluteTolerance(10000);
    if(flipper.getState() == flippingState){
      controller.setSetpoint(elevatorState.getHeight());
    }else{
      controller.setSetpoint((RobotMap.ROBOT_FLIP_SAFE_ZONE_BOTTOM + RobotMap.ROBOT_FLIP_SAFE_ZONE_TOP)/2);
    }
    controller.setOutputRange(-0.6, 0.6);
    controller.enable();
    
  }

  @Override
  protected void execute() {
    if(elevator.getHeight() > RobotMap.ROBOT_FLIP_SAFE_ZONE_BOTTOM && elevator.getHeight() < RobotMap.ROBOT_FLIP_SAFE_ZONE_TOP){
      controller.setSetpoint(elevatorState.getHeight());
      controller.disable();
      int direction = elevator.getencoder().get() > elevatorState.getHeight()?-1:1;
      elevator.move(0.05*direction);
      
    }else{
      controller.enable();
    }
  }

  @Override
  protected boolean isFinished() {
    return controller.onTarget() || elevator.disable || IO.isAutoDisabled() || !elevator.canGoUp();
  }

  @Override
  protected void end() {
    controller.disable();
    elevator.Stop();
    elevator.auto = false;
    elevator.setState(elevatorState);
    flipper.setState(flippingState);
    Robot.height = height;
    Robot.compressor.setClosedLoopControl(true);

    
  }

  @Override
  protected void interrupted() {
    end();
  }
}
