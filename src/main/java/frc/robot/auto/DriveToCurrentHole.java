/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.auto;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.IO; 
import frc.robot.KoiEncoderFollower;
import frc.robot.RobotMap;
import jaci.pathfinder.Pathfinder;
import jaci.pathfinder.Trajectory;
import jaci.pathfinder.Waypoint;
import jaci.pathfinder.modifiers.TankModifier;

import static frc.robot.Robot.driver;

public class DriveToCurrentHole extends Command {

  private Trajectory trajectory;
  private TankModifier modifier;
  private KoiEncoderFollower left,right;

  private double p = 0.01;
  private double i = 0.0;
  private double d = 0.9;
  private double acc = 0;
  private double vel = 1;

  public  DriveToCurrentHole(double x, double y, double angle) {
    requires(driver);
    driver.resetEncoders();
    driver.resetGyro();
    y += 34.5;
    

  }

  @Override
  protected void initialize() {
    driver.setAuto(true);
    System.out.println("driving...");
    driver.resetGyro();
    driver.resetEnc();
    Waypoint[] points = new Waypoint[] {
      new Waypoint(0,0,0),
      //new Waypoint(2,0,Pathfinder.d2r(0))
      new Waypoint((SmartDashboard.getEntry("nearDriveDistance").getDouble(0.1)/100) - 0.06, 0.345, Pathfinder.d2r(SmartDashboard.getEntry("turnAngle").getDouble(0)))                       
    };
    Trajectory.Config config = new Trajectory.Config(Trajectory.FitMethod.HERMITE_CUBIC, Trajectory.Config.SAMPLES_FAST, 0.02, vel, 2.0, 90.0);
    trajectory = Pathfinder.generate(points, config);
    System.out.println("created!");

    modifier = new TankModifier(trajectory).modify(0.63);

    left = new KoiEncoderFollower(modifier.getLeftTrajectory());
    right = new KoiEncoderFollower(modifier.getRightTrajectory());

    left.configureEncoder(driver.getLeftEncoder().get(), RobotMap.ROBOT_DRIVING_TRANSMISSION_VALUE/RobotMap.DRIVING_SYSTEM_COUNT_PER_REV, RobotMap.WHEEL_DIAMETER);

    left.configurePIDVA(p, i, d, 1 / vel, acc);

    right.configureEncoder(driver.getRightEncoder().get(), 360, RobotMap.WHEEL_DIAMETER);

    right.configurePIDVA(p, i, d, 1 / vel, acc);
    driver.setDirection(-1);
  }

  @Override
  protected void execute() {
    System.out.println("fasdfasdf");
    double l = left.calculate(driver.getLeftEncoder().getDistance());
    double r = right.calculate(driver.getRightEncoder().getDistance());

    double gyro_heading = driver.getGyro().getAngle();    // Assuming the gyro is giving a value in degrees
    double desired_heading = Pathfinder.r2d(left.getHeading());  // Should also be in degrees

    // This allows the angle difference to respect 'wrapping', where 360 and 0 are the same value
    double angleDifference = Pathfinder.boundHalfDegrees(desired_heading - gyro_heading);
    angleDifference = angleDifference % 360.0;
    if (Math.abs(angleDifference) > 180.0) {
      angleDifference = (angleDifference > 0) ? angleDifference - 360 : angleDifference + 360;
    } 

    double turn = 0.8 * (-1.0/80.0) * angleDifference;

    driver.drive(l + turn, r - turn);
    
  }

  @Override
  protected boolean isFinished() {
    return left.isFinished() || right.isFinished() || IO.isAutoDisabled();
  }

  @Override
  protected void end() {
    driver.setAuto(false);
    driver.stop();
  }

  @Override
  protected void interrupted() {
      end();
  }
}
