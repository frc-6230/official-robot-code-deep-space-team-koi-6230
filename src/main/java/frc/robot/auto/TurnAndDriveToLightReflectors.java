/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.auto;

import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.commands.Wait;

import static frc.robot.Robot.driver;

public class TurnAndDriveToLightReflectors extends CommandGroup {
  /**
   * Add your docs here.
   */
  public TurnAndDriveToLightReflectors() {
    addSequential(new DriveToCurrentHole(SmartDashboard.getEntry("nearDriveDistance").getDouble(0),0,SmartDashboard.getEntry("turnAngle").getDouble(0)));
  }

}
