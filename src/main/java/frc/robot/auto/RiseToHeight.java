/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.auto;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.robot.auto.AutoElevator;
import frc.robot.ElevatorHeights;
import static frc.robot.Robot.flipper;

public class RiseToHeight extends CommandGroup {


  public RiseToHeight(ElevatorHeights height) {
    //flipper.getCurrentCommand().setState(height.getFlippingState());
    addParallel(new AutoElevator(height));
  }
}
