/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.auto.TurnAndDriveToLightReflectors;
import frc.robot.auto.TurnAndDriveToLightReflectors;
import frc.robot.auto.TurnCommand;
import frc.robot.commands.ChangeDirection;
import frc.robot.commands.CloseCollector;
import frc.robot.commands.CollectorChangeState;
import frc.robot.commands.GripChangeState;
import frc.robot.commands.HatchPanelCommand;
import frc.robot.commands.OpenCollector;

/**
 * Add your docs here.
 */

 
public class IO {
    public static Joystick driveJoy,elevatorJoy,board;


    public static void init(){
        board = new Joystick(2);

        try{
            driveJoy = new Joystick(0);

            new JoystickButton(driveJoy, 4).whenPressed(new ChangeDirection());
            new JoystickButton(driveJoy, 6).whenPressed(new HatchPanelCommand());
            //new JoystickButton(driveJoy, 3).whenPressed(new CollectorChangeState());
            new JoystickButton(driveJoy, 5).whenPressed(new GripChangeState());
        

            SmartDashboard.putString("Main Joystick: ", "Connected!");
        }catch(Exception e){
            SmartDashboard.putString("Main Joystick: ", "Disconnected!");
        }

        try{
            elevatorJoy = new Joystick(1);
            new JoystickButton(elevatorJoy, 3).whenPressed(new CloseCollector());
            new JoystickButton(elevatorJoy, 4).whenPressed(new OpenCollector());
            SmartDashboard.putString("Elevator Joystick", "Connected!");
        }catch(Exception e){
            SmartDashboard.putString("Elevator Joystick", "Disconnected!");
        }

       

        // new JoystickButton(board, 1).whenPressed(new RiseToHeight(ElevatorHeights.ROCKET_TOP_BALL));
        // new JoystickButton(board, 2).whenPressed(new RiseToHeight(ElevatorHeights.ROCKET_MIDDLE_BALL));
        // new JoystickButton(board, 3).whenPressed(new RiseToHeight(ElevatorHeights.ROCKET_BOTTOM_BALL));
        // new JoystickButton(board, 4).whenPressed(new RiseToHeight(ElevatorHeights.CARGO_BALL));
        // new JoystickButton(board, 5).whenPressed(new RiseToHeight(ElevatorHeights.ROCKET_TOP_HATCH));
        // new JoystickButton(board, 6).whenPressed(new RiseToHeight(ElevatorHeights.ROCKET_MIDDLE_HATCH));
        // new JoystickButton(board, 7).whenPressed(new RiseToHeight(ElevatorHeights.ROCKET_BOTTOM_HATCH));
         new JoystickButton(board, 9).whenPressed(new TurnCommand(2));
         new JoystickButton(board, 10).whenPressed(new TurnCommand(1));
         new JoystickButton(board, 11).whenPressed(new TurnCommand(0));
         new JoystickButton(board, 12).whenPressed(new TurnAndDriveToLightReflectors());


    }
    public static boolean is3Pressed(){
        try{
            boolean temp = board.getRawAxis(0) == -1;
            return temp;
        }catch(Exception e){
            return false;
        }
    }

    public static double getLeftAxis(){
        try{
            double temp = driveJoy.getRawAxis(1);
            
            SmartDashboard.putString("Main Joystick: ", "Connected!");

            return temp;

        }
        catch(Exception e){
            SmartDashboard.putString("Main Joystick: ", "Disconnected!");
            return 0;
        }
    }


    public static double getRightAxis(){
        try{
            double temp = driveJoy.getRawAxis(5);
            
            SmartDashboard.putString("Main Joystick: ", "Connected!");

            return temp;

        }
        catch(Exception e){
            SmartDashboard.putString("Main Joystick: ", "Disconnected!");
            return 0;
        }
    }


    public static double getElevatorAxis(){
        try{
            double temp = -elevatorJoy.getRawAxis(1);
            
            SmartDashboard.putString("Elevator Joystick: ", "Connected!");

            return temp;

        }
        catch(Exception e){
            SmartDashboard.putString("Elevator Joystick: ", "Disconnected!");
            return 0;
        }
    }

    public static double getCollectorAxis(){
        try{
            double temp = driveJoy.getRawAxis(3);
            
            SmartDashboard.putString("Main Joystick: ", "Connected!");

            return temp;

        }
        catch(Exception e){
            SmartDashboard.putString("Main Joystick: ", "Disconnected!");
            return 0;
        } 
    }

    public static double getFlipAxis(){
        try{
            double temp = (((elevatorJoy.getRawAxis(3) * - 1) + 1)/2)*180;
            
            SmartDashboard.putString("Elevator Joystick: ", "Connected!");

            return temp;

        }
        catch(Exception e){
            SmartDashboard.putString("Elevator Joystick: ", "Disconnected!");
            return -999;
        }
    }
    public static boolean isAutoDisabled(){
        try{
            boolean temp = board.getRawButton(8);
            return temp;
        }catch(Exception e){
            return true;
        }
    }

    public static boolean isFilpTeleop(){
        try{
            boolean temp = driveJoy.getRawButton(3);
            return temp;
        }catch(Exception e){
            return false;
        }
    }

    public static boolean getSlowButton(){
        try{
            boolean temp = driveJoy.getRawButton(4);
            return temp;
        }catch(Exception e){
            return false;
        }
    }

    public static double getClimbAxis(){
        return ((elevatorJoy.getRawAxis(3)*-1) * 2)-1;
    }

    public static double getFlippingAxis(){
        return elevatorJoy.getRawAxis(2);
    }







}
