/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * Add your docs here.
 */
public enum ElevatorState {
    Collect(0),
    BottomBall(73870),
    MiddleBall(245580),
    TopBall(340000),
    BottomHatch(0),
    MiddleHatch(189459),
    TopHatch(330000),
    CargoBall(0);

    private double height;

    private ElevatorState(double height){
        this.height = height;
    }

    public double getHeight(){
        return height;
    }


}
