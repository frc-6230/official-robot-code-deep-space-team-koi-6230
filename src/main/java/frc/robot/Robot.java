/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;
import edu.wpi.cscore.UsbCamera;
import edu.wpi.cscore.VideoSink;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.subsystems.*;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.DriverStation.Alliance;

public class Robot extends TimedRobot{

  public static LcdDisplay lcdDisplay;
  public static final DrivingSystem driver = new DrivingSystem();
  public static final CollectingSystem collector = new CollectingSystem();
  public static final ElevatorSystem elevator = new ElevatorSystem();
  public static final FlippingSystem flipper = new FlippingSystem();
  public static final GripSystem grip = new GripSystem();
  public static final ClimbSystem climber = new ClimbSystem();
  public static ElevatorHeights height = ElevatorHeights.COLLECT_POSITION;
  public static Compressor compressor;
  public static boolean flipAuto;
  UsbCamera camera1,camera2,camera3;
  VideoSink server;

  public SendableChooser<String> chooser;

  @Override
  public void robotInit() {
     //camera2 = CameraServer.getInstance().startAutomaticCapture(1);
     //camera3 = CameraServer.getInstance().startAutomaticCapture(2);
     camera1 = CameraServer.getInstance().startAutomaticCapture(0);
     camera1.setFPS(30);
     //camera2.setFPS(30);
     //camera3.setFPS(30);
     server = CameraServer.getInstance().getServer();
      
    IO.init();
    
    compressor = new Compressor();
    compressor.setClosedLoopControl(true);
    driver.changeColor(153,50,204);

    // chooser = new SendableChooser<>();
    // chooser.addOption("Hatch Panel", "H");
    // chooser.addOption("Cargo", "B");
    // chooser.addOption("None", "D");
  }

  @Override
  public void robotPeriodic() {
  }



  @Override
  public void disabledInit() {
  }

  @Override
  public void disabledPeriodic() {
    Scheduler.getInstance().run();
  }

 
  @Override
  public void autonomousInit() {
    lcdDisplay = new LcdDisplay();
    lcdDisplay.initLCD();
    if(DriverStation.getInstance().getAlliance() == Alliance.Blue){
      lcdDisplay.LCDwriteString("BLUE Alliance!!!", 1);
      lcdDisplay.LCDwriteString("Team Koi #6230", 2);
    }else{
      lcdDisplay.LCDwriteString("RED Alliance!!!", 1);
      lcdDisplay.LCDwriteString("Team Koi #6230", 2);
    }
    //flipAuto = true;
    driver.setDirection(1);
    elevator.getencoder().reset();
    flipper.getEncoder().reset();
    // if(chooser.getSelected().equals("H")){
    //   flipper.setOffset(90);
    //   flipper.setState(FlippingState.Closed);
    // }else if(chooser.getSelected().equals("B")){
    //   flipper.setOffset(-45);
    //   flipper.setState(FlippingState.Collect);
    // }
    
    height = ElevatorHeights.COLLECT_POSITION;

    //new AutoElevator(57684).start();
    // double angle = calculateAngle();
    // double distance = driver.getDistanceFromObject();
    // double frontDistance = Math.cos(angle) * distance;
    // new DriveToCurrentHole(frontDistance, distance, angle).start();
    //new FlipSystemCommand(180, 0).start();
    // new DrivePath(path).start();
    //new TurnCommand(SmartDashboard.getEntry("IP_Angle").getDouble(0)).start();
  }

  @Override
  public void autonomousPeriodic() {
    //if(IO.is3Pressed()) new TurnCommand(2).start();
    //if(IO.isFilpTeleop()) flipAuto = !flipAuto;
    SmartDashboard.putNumber("eLEVATOR:", elevator.getencoder().get());
    SmartDashboard.putNumber("Rotation Angle:", flipper.getAngle());
    SmartDashboard.putBoolean("Grip:", grip.getHolder().get() == Value.kReverse);
    // System.out.println("number:" + SmartDashboard.getEntry("IP_Angle").getNumber(0));
    // SmartDashboard.putNumber("left", driver.leftEncoder.getDistance());
    // SmartDashboard.putNumber("right", driver.rightEncoder.getDistance());
    Scheduler.getInstance().run();
  }

  @Override
  public void teleopInit() {
    //flipAuto = true;
    elevator.getencoder().reset();
    flipper.getEncoder().reset();
    // if(chooser.getSelected().equals("H")){
    //   flipper.setOffset(90);
    //   flipper.setState(FlippingState.Closed);
    // }else if(chooser.getSelected().equals("B")){
    //   flipper.setOffset(-45);
    //   flipper.setState(FlippingState.Collect);
    // }
    flipper.setState(FlippingState.Collect);
    height = ElevatorHeights.COLLECT_POSITION;
    

    
  }

  @Override
  public void teleopPeriodic() {
    if(IO.elevatorJoy.getRawButton(5)){
      climber.move(0.7);
    }
    else if(IO.elevatorJoy.getRawButton(6)){
      climber.move(-0.7);
    }else{
      climber.move(0);
    }

    
    // SmartDashboard.putNumber("final angle", Math.toDegrees(Math.atan((34.5 + (Math.tan(Math.toRadians(SmartDashboard.getEntry("IP_Angle0").getDouble(0))))*SmartDashboard.getEntry("Distance0").getDouble(0))/SmartDashboard.getEntry("Distance0").getDouble(0))));
      // if(IO.driveJoy.getRawButton()){
      //   new Thread(new Runnable(){
    
      //     @Override
      //     public void run() {
      //      while(true){
      //       new Thread(new Runnable(){
        
      //         @Override
      //         public void run() {
      //           double a = 0;
      //           while(true){
      //             for (int i = 0; i < 100000000; i++) {
      //               for(int j = 0;j < 100000000; j++){
      //                 for(int y = 0; y< 10000000;y++){
      //                   a = Math.pow(32, Math.sqrt(Math.pow(Math.PI, Math.sqrt(Math.PI))));
      //                 }
      //               }
      //             }
                  
      //           }
      //         }
      //       });
      //      } 
      //     }
      //   }).start();
      // }
  //  if(IO.is3Pressed()) new TurnCommand(2).start();
//    if(IO.isFilpTeleop()) flipAuto = !flipAuto;
    SmartDashboard.putNumber("eLEVATOR:", elevator.getencoder().get());
    SmartDashboard.putNumber("Rotation Angle:", flipper.getAngle());
    SmartDashboard.putBoolean("Grip:", grip.getHolder().get() == Value.kReverse);

    //elevator.move(IO.getElevatorAxis());
    //flipper.rotate(0.5);
    //SmartDashboard.putNumber("FLIP",flipper.getAngle());
    //SmartDashboard.putNumber("distance", driver.leftEncoder.getDistance());
    Scheduler.getInstance().run();
    // System.out.println(driver.leftEncoder.get());
    // SmartDashboard.putNumber("flip", flipper.getEncoder().get());
  }

  @Override
  public void testInit(){
    lcdDisplay = new LcdDisplay();
    lcdDisplay.initLCD();

    lcdDisplay.LCDwriteString("---------ROKI--------", 1);  
    lcdDisplay.LCDwriteString("Team KOI #6230", 2);
    Timer.delay(5);
    
  }

  public static boolean inRange(double current,double expected,double offset){
    return current - offset >= expected && current + offset <= expected;
  }
 
  @Override
  public void testPeriodic() {
      lcdDisplay.clear();
      lcdDisplay.makeAutoTest();
      lcdDisplay.printProblems();   
      Timer.delay(10);
  }
}
