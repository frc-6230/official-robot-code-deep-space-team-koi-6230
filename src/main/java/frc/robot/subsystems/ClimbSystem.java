/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.commands.ClimbCommand;

/**
 * Add your docs here.
 */
public class ClimbSystem extends Subsystem {
  private VictorSP motor;

  public ClimbSystem(){
    motor = new VictorSP(RobotMap.CLIMB_SYSTEM_VICTOR);
  }

  public void stop(){
    motor.set(0);
  }
  
  public void move(double speed){
    motor.set(speed);
  }

  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new ClimbCommand());
  }
}
