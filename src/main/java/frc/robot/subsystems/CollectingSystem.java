/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.commands.CollectCommand;

/**
 * Add your docs here.
 */
public class CollectingSystem extends Subsystem {

  //Robot Victors:
  private VictorSP motor;

  private DoubleSolenoid solenoid;
  
  public CollectingSystem(){
    motor=new VictorSP(RobotMap.COLLECTING_SYSTEM_VICTOR);
    solenoid = new DoubleSolenoid(RobotMap.COLLECTING_SYSTEM_SOLENOID_CHANNELS[0], RobotMap.COLLECTING_SYSTEM_SOLENOID_CHANNELS[1]);
  }

  public void collect(double speed){
    motor.set(speed*0.2);
  }
  public void open(){
    if(solenoid.get() != Value.kForward){
      solenoid.set(DoubleSolenoid.Value.kForward);
      
    }
  }

  public void close(){
    if(solenoid.get() != Value.kReverse){
      solenoid.set(DoubleSolenoid.Value.kReverse);
    }
  }


  public void changeState(){
    if(solenoid.get() != Value.kForward){
      solenoid.set(DoubleSolenoid.Value.kForward);
      
    }
    else if(solenoid.get() != Value.kReverse){
      solenoid.set(DoubleSolenoid.Value.kReverse);
    }
  }




  public void stop(){
    motor.set(0);
  }
  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new CollectCommand());
  }
}
