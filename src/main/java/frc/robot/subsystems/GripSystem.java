/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.RobotMap;
import frc.robot.commands.BallListenerCommand;

/**
 * Add your docs here.
 */
public class GripSystem extends Subsystem {
  
  private DoubleSolenoid holder;
  private DoubleSolenoid push;
  private AnalogInput ballSensor;
  private boolean enableUltra;

  public GripSystem(){
    holder = new DoubleSolenoid(RobotMap.GRIP_SYSTEM_SOLENOID_CHANNELS[0], RobotMap.GRIP_SYSTEM_SOLENOID_CHANNELS[1]);
    push = new DoubleSolenoid(RobotMap.GRIP_SYSTEM_SOLENOID_CHANNELS[2],RobotMap.GRIP_SYSTEM_SOLENOID_CHANNELS[3]);
    enableUltra = true;
    ballSensor = new AnalogInput(RobotMap.COLLECTING_SYSTEM_ULTRASONIC_CHANNEL);
  }

  public DoubleSolenoid getHolder(){
    return holder;
  }

  public void changeState(){
    if(holder.get() != Value.kReverse){
      holder.set(Value.kReverse);
      if(push.get() != Value.kForward){
        push.set(Value.kForward);
      }
    }
    
    else if(holder.get() != Value.kForward){
      holder.set(Value.kForward);
    }
  }
  public void close(){
    if(holder.get() != Value.kForward){
      holder.set(Value.kForward);
    }
  }

  public void reg(){
    if(holder.get() != Value.kReverse){
      holder.set(Value.kReverse);
      if(push.get() != Value.kForward){
        push.set(Value.kForward);
      }

    }
    
    else if(holder.get() != Value.kForward){
      holder.set(Value.kForward);
      if(push.get() != Value.kReverse){
        push.set(Value.kReverse);
      }
    }
  }
  

  public boolean ballCaught(){
    SmartDashboard.putString("ultra", ballSensor.getVoltage() + "");
    return ballSensor.getVoltage() <= 0.33 && enableUltra;
  }

  public void setUltra(boolean bool){
    enableUltra = bool;
  }

  public void closePush(){
    if(push.get() != Value.kReverse){
      push.set(Value.kReverse);
    }

  }

  @Override
  public void initDefaultCommand() {
    //setDefaultCommand(new BallListenerCommand());
  }
}
