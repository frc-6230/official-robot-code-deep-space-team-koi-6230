/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;


import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.CounterBase.EncodingType;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.ElevatorState;
import frc.robot.RobotMap;
import frc.robot.commands.MoveElevatorCommand;

/**
 * Add your docs here.
 */
public class ElevatorSystem extends Subsystem {
  private VictorSP leftMotor, rightMotor;
  private Encoder encoder;
  public boolean auto = false;
  public boolean disable = false;
  private ElevatorState state;
  private DigitalInput limitSwitch;
  
  public ElevatorSystem(){
    leftMotor=new VictorSP(RobotMap.ELEVATOR_SYSTEM_LEFT_VICTOR);
    rightMotor=new VictorSP(RobotMap.ELEVATOR_SYSTEM_RIGHT_VICTOR);

    encoder=new Encoder(RobotMap.ELEVATOR_SYSTEM_ENCODER_CHANNELS[0],RobotMap.ELEVATOR_SYSTEM_ENCODER_CHANNELS[1],true,EncodingType.k4X);
    encoder.reset();
    state = ElevatorState.Collect;

    limitSwitch = new DigitalInput(RobotMap.ELEVATOR_LIMIT_SWITCH);

    rightMotor.setInverted(true);


  }

  public ElevatorState getState(){
    return state;
  }

  public void setState(ElevatorState state){
    this.state = state;
  }

  public Encoder getencoder(){
    return encoder;
  }
  public void move(double speed){
    
    //if(speed > 0 && canGoUp()){
      leftMotor.set(speed);
      rightMotor.set(speed);
    //}
    SmartDashboard.putNumber("Elevator Speed:", speed);
  }

  public boolean canGoUp(){
    return limitSwitch.get();
  }

  public double getHeight(){
    return encoder.get();
  }

  public void Stop(){
    leftMotor.set(0);
    rightMotor.set(0);
  }


  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new MoveElevatorCommand());

  }
}
