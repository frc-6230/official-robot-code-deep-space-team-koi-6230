/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.FlippingState;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.FlipSystemCommand;

/**
 * Add your docs here.
 */
public class FlippingSystem extends Subsystem {
  
  private VictorSP rotationMotor;
  private Encoder rotationEncoder;
  private boolean onTarget;
  private FlippingState state;
  private double offSet;
  private double angle;
  

  public FlippingSystem(){
    rotationMotor = new VictorSP(RobotMap.FLIPPING_SYSTEM_VICTOR);

    //Encoder setup
    rotationEncoder = new Encoder(RobotMap.ROTATION_SYSTEM_ENCODER_CHANNELS[0], RobotMap.ROTATION_SYSTEM_ENCODER_CHANNELS[1],false);
    rotationEncoder.setDistancePerPulse(360/(RobotMap.ROTATION_SYSTEM_COUNT_PER_REV*71));
    rotationEncoder.reset();
    onTarget = true;

    rotationMotor.setInverted(true);
    state = FlippingState.Collect;
    offSet = -45;
    angle = state.getAngle();
  }

  public void setAngle(double angle){

  }

  public double getsetAngle(){
    return angle;
  }

  public boolean isOnRightAngle(){
    return Robot.inRange(getAngle(),state.getAngle(),8);
  }


  public void setOnTarget(boolean bool){
    onTarget = bool;
  }

  public FlippingState getState(){
    return state;
  }

  public void setState(FlippingState state){
    this.state = state;
  }


  public void rotate(double speed){
    rotationMotor.set(speed);
  }

  public double getAngle(){
    return rotationEncoder.getDistance() + offSet;
  }

  public void stop(){
    rotationMotor.set(0);
  }

  public void reset(){
    rotationEncoder.reset();
  }

  public Encoder getEncoder(){
    return rotationEncoder;
  }

  public void setOffset(double f){
    offSet = f;
  }

  public VictorSP getVictor(){
    return rotationMotor;
  }

  @Override
  public void initDefaultCommand() {
   setDefaultCommand(new FlipSystemCommand(state));
  }
}
