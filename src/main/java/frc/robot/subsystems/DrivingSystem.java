/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;


import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.PWM;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.CounterBase.EncodingType;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.RobotMap;
import frc.robot.commands.DriveCommand;

/**
 * Add your docs here.
 */
public class DrivingSystem extends Subsystem {
  
  // Robot Victors:
  private VictorSP rightmotor,leftmotor;
  public Encoder leftEncoder, rightEncoder;
  private ADXRS450_Gyro gyro;
  private AnalogInput ultrasonic;
  private int direction = 1;
  private PWM r,g,b;
  public static final double JERK = 0.007;
  private double lastTime;
  private double lastLeft,lastRight;
  private double max = 0;
  private boolean auto = false;


  public DrivingSystem (){
    rightmotor=new VictorSP(RobotMap.DRIVING_SYSTEM_RIGHT_VICTOR);
    leftmotor=new VictorSP(RobotMap.DRIVING_SYSTEM_LEFT_VICTOR);

    gyro = new ADXRS450_Gyro();
    gyro.calibrate();
  
    leftEncoder = new Encoder(RobotMap.DRIVING_SYSTEM_ENCODER_LEFT_CHANNELS[0],RobotMap.DRIVING_SYSTEM_ENCODER_LEFT_CHANNELS[1], true, EncodingType.k4X);
    rightEncoder = new Encoder(RobotMap.DRIVING_SYSTEM_ENCODER_RIGHT_CHANNELS[0],RobotMap.DRIVING_SYSTEM_ENCODER_RIGHT_CHANNELS[1], false, EncodingType.k4X);

    leftEncoder.setDistancePerPulse(RobotMap.WHEEL_PERIMETER/RobotMap.ROBOT_DRIVING_TRANSMISSION_VALUE/RobotMap.DRIVING_SYSTEM_COUNT_PER_REV);
    rightEncoder.setDistancePerPulse(RobotMap.WHEEL_PERIMETER/360);

    rightmotor.setInverted(true);
    //leftmotor.setInverted(true);
    lastLeft = lastRight = 0;

    ultrasonic = new AnalogInput(RobotMap.DRIVING_SYSTEM_ULTRASONIC_CHANNEL);

    resetEncoders();

    // r = new PWM(0);
    // g = new PWM(1);
    // b = new PWM(2);
    lastTime = 0;
  }

  public boolean isAuto(){
    return auto;
  }


  public void setAuto(boolean b){
    auto = b;
  }

  public void resetEnc(){
    leftEncoder.reset();
    rightEncoder.reset();
  }

  public double getDistanceFromObject(){
    return ultrasonic.getVoltage()*102.4;
  }

  public void changeColor(int red,int green, int blue){
    // r.setRaw(red);
    // g.setRaw(green);
    // b.setRaw(blue);
  }


  public Encoder getLeftEncoder(){
    return leftEncoder;
  }

  public Encoder getRightEncoder(){
    return rightEncoder;
  }

  //driving method
  public void drive(double leftspeed, double rightspeed){
    lastTime = Timer.getFPGATimestamp() - lastTime;
    double leftJerk = (leftspeed - lastLeft)/lastTime;
    double rightJerk = (rightspeed - lastRight)/lastTime; 
    
    
    max = Math.max(max, leftJerk);
    
    SmartDashboard.putNumber("Left", leftEncoder.getDistance());
    
    SmartDashboard.putNumber("Right", rightEncoder.getDistance());
    if(Math.abs(leftJerk) > JERK){
      leftspeed = JERK * lastTime + lastLeft;
      if(leftJerk < 0)
        leftspeed = -JERK * lastTime + lastLeft;
    }

    if(Math.abs(rightJerk) > JERK){
      rightspeed = JERK * lastTime + lastRight;
      if(rightJerk < 0){
        rightspeed = -JERK * lastTime + lastRight;
      }
    }

    if(direction == 1){
      rightmotor.set(leftspeed*direction);
      leftmotor.set(rightspeed*direction);
    }else{
      rightmotor.set(rightspeed*direction);
      leftmotor.set(leftspeed*direction);
    }

    lastLeft = leftspeed;
    lastRight = rightspeed;
    
    
  }

  public void setDirection(int direction){
    this.direction = direction;
  }

  public int getDirection(){
    return direction;
  }

  public void resetGyro(){
    gyro.reset();
  }

  public void resetEncoders(){
    rightEncoder.reset();
    leftEncoder.reset();
  }

  public double getDistance(){
    return (rightEncoder.getDistance() + leftEncoder.getDistance())/2;
    
  }

  public ADXRS450_Gyro getGyro(){
    return gyro;
  }


  //stop
  public void stop(){
    rightmotor.set(0);
    leftmotor.set(0);
  }
  
  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new DriveCommand());
  }
}
