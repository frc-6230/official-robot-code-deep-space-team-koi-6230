/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * Add your docs here.
 */
public enum ElevatorHeights{
    ROCKET_BOTTOM_BALL(ElevatorState.BottomBall,FlippingState.Place),
    ROCKET_MIDDLE_BALL(ElevatorState.MiddleBall,FlippingState.Place),
    ROCKET_TOP_BALL(ElevatorState.TopBall,FlippingState.Place),
    ROCKET_BOTTOM_HATCH(ElevatorState.BottomHatch, FlippingState.Default),
    ROCKET_MIDDLE_HATCH(ElevatorState.MiddleHatch,  FlippingState.Default),
    ROCKET_TOP_HATCH(ElevatorState.TopHatch,  FlippingState.Default),
    CARGO_BALL(ElevatorState.CargoBall, FlippingState.Throw),
    COLLECT_POSITION(ElevatorState.Collect, FlippingState.Collect);

    private final ElevatorState elevatorState;
    private final FlippingState flippingState;

    private ElevatorHeights(ElevatorState elevatorState, FlippingState flippingState){
        this.elevatorState = elevatorState;
        this.flippingState = flippingState;
    }

    public ElevatorState getElevatorState(){
        return elevatorState;
    }

    public FlippingState getFlippingState(){
        return flippingState;
    }

}