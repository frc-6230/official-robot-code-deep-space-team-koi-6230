/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {

  //Victors ports:
  public static final int DRIVING_SYSTEM_LEFT_VICTOR = 0;
  public static final int DRIVING_SYSTEM_RIGHT_VICTOR = 1;
  public static final int ELEVATOR_SYSTEM_LEFT_VICTOR = 4;
  public static final int ELEVATOR_SYSTEM_RIGHT_VICTOR = 3;
  public static final int COLLECTING_SYSTEM_VICTOR = 2;
  public static final int FLIPPING_SYSTEM_VICTOR = 5;
  public static final int CLIMB_SYSTEM_VICTOR = 6;

  //Encoders:
  public static final int[] ROTATION_SYSTEM_ENCODER_CHANNELS = {4,5};
  public static final int[] DRIVING_SYSTEM_ENCODER_LEFT_CHANNELS = {3,2};
  public static final int[] DRIVING_SYSTEM_ENCODER_RIGHT_CHANNELS = {0,1};
  public static final int[] ELEVATOR_SYSTEM_ENCODER_CHANNELS = {6,7};

  //MicroSwiches:
  public static final int ELEVATOR_LIMIT_SWITCH = 8;

//Encoder pulses:
  public static final double ROTATION_SYSTEM_COUNT_PER_REV = 7; //PG71
  public static final double DRIVING_SYSTEM_COUNT_PER_REV = 20;
  public static final double ELEVATOR_SYSTEM_COUNT_PER_REV = 20;

  //Solenoids:
  public static final int[] COLLECTING_SYSTEM_SOLENOID_CHANNELS = {4,5};
  public static final int[] GRIP_SYSTEM_SOLENOID_CHANNELS = {2,3,0,1};

  //ultrasonics:
  public static final int COLLECTING_SYSTEM_ULTRASONIC_CHANNEL = 0;
  public static final int DRIVING_SYSTEM_ULTRASONIC_CHANNEL = 1;

  //robot stats:
  public static final double ROBOT_DRIVING_TRANSMISSION_VALUE = 10.75;
  public static final double WHEEL_DIAMETER = 6 * 2.54/100; // inch to meter
  public static final double WHEEL_PERIMETER = WHEEL_DIAMETER * Math.PI;
  public static final double ROBOT_FLIP_SAFE_ZONE_BOTTOM = 80000;
  public static final double ROBOT_FLIP_SAFE_ZONE_TOP = 110000; 




}
