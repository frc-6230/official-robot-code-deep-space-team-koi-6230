/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;


import java.util.ArrayList;

/**
 * Add your docs here.
 */

import static frc.robot.Robot.driver;
import static frc.robot.Robot.elevator;
import static frc.robot.Robot.flipper;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.PowerDistributionPanel;

public class LcdDisplay {

	/* **********************************************************
	 *      Constants for LCD Panel
	 * ********************************************************/
	// LCD Commands
	private static final int LCD_CLEARDISPLAY = 0x01;
	private static final int LCD_RETURNHOME = 0x02;
	private static final int LCD_ENTRYMODESET = 0x04;
	private static final int LCD_DISPLAYCONTROL = 0x08;
	private static final int LCD_CURSORSHIFT = 0x10;
	private static final int LCD_FUNCTIONSET = 0x20;
	private static final int LCD_SETCGRAMADDR = 0x40;
	private static final int LCD_SETDDRAMADDR = 0x80;

	// Flags for display on/off control
	private static final int LCD_DISPLAYON = 0x04;
	private static final int LCD_DISPLAYOFF = 0x00;
	private static final int LCD_CURSORON = 0x02;
	private static final int LCD_CURSOROFF = 0x00;
	private static final int LCD_BLINKON = 0x01;
	private static final int LCD_BLINKOFF = 0x00;

	// Flags for display entry mode
	// private static final int LCD_ENTRYRIGHT = 0x00;
	private static final int LCD_ENTRYLEFT = 0x02;
	private static final int LCD_ENTRYSHIFTINCREMENT = 0x01;
	private static final int LCD_ENTRYSHIFTDECREMENT = 0x00;

	// Flags for display/cursor shift
	private static final int LCD_DISPLAYMOVE = 0x08;
	private static final int LCD_CURSORMOVE = 0x00;
	private static final int LCD_MOVERIGHT = 0x04;
	private static final int LCD_MOVELEFT = 0x00;

	// flags for function set
	private static final int LCD_8BITMODE = 0x10;
	private static final int LCD_4BITMODE = 0x00;
	private static final int LCD_2LINE = 0x08;	//for 2 or 4 lines actually
	private static final int LCD_1LINE = 0x00;
	private static final int LCD_5x10DOTS = 0x04;	//seldom used!!
	private static final int LCD_5x8DOTS = 0x00;	

	// flags for backlight control
	private static final int LCD_BACKLIGHT = 0x08;
	private static final int LCD_NOBACKLIGHT = 0x00;

	//bitmasks for register control
	private static final int En = 0b00000100; // Enable bit
	private static final int Rw = 0b00000010; // Read/Write bit
	private static final int Rs = 0b00000001; // Register select bit

	/* *********************************************************************************
	 *      End of LCD constants
	 *  ********************************************************************************/

	//	instance variables
	I2C lcdDisplay;


	private PowerDistributionPanel powerDistributionPanel;
    
	private ArrayList<String> problems;

	private boolean testing = false;
	private Thread loadThread;
	
	long startTime = 0;

	/* ***************************************************************************
	 *      Methods for using LCD Display
	 * **************************************************************************/

	public void initLCD() {
		
		lcdDisplay = new I2C(I2C.Port.kOnboard, 0x27);
		problems = new ArrayList<String>();

		LCDwriteCMD(0x03);
		LCDwriteCMD(0x03); 
		LCDwriteCMD(0x03); 
		LCDwriteCMD(0x02);
		//4 bit mode??? -- yes. Always. It's the default way of doing this for LCD displays
		LCDwriteCMD( LCD_FUNCTIONSET | LCD_2LINE | LCD_5x8DOTS | LCD_4BITMODE );
		LCDwriteCMD( LCD_DISPLAYCONTROL | LCD_DISPLAYON );    
		LCDwriteCMD( LCD_CLEARDISPLAY );
		LCDwriteCMD( LCD_ENTRYMODESET | LCD_ENTRYLEFT );
		zsleep(10);
		
	}

	//write a sleep method to get rid of the try-catch stuff
	private void zsleep(int t) {
		try { Thread.sleep(t);
		} catch(InterruptedException e) {}
	}

	//This is for writing commands, 4 bits at a time 
	private void LCDwriteCMD (int data) {
		LCD_rawWrite(data & 0xF0);
		LCD_rawWrite((data <<4 ) & 0xF0);
	}

	//This is for writing a character, 4 bits at a time
	private void LCDwriteChar ( int data) {
		LCD_rawWrite( Rs |  (data & 0xF0));
		LCD_rawWrite( Rs | ((data <<4 ) & 0xF0));
	}

	private void LCD_rawWrite( int data) {
		lcdDisplay.write(0, data | LCD_BACKLIGHT );
		strobe(data);
	}

	private void strobe(int data){
		//    	Syntax: lcdDisplay.write(reg,data);    	
		lcdDisplay.write(0, data | En | LCD_BACKLIGHT );
		zsleep(1);
		lcdDisplay.write(0, (data & ~En) | LCD_BACKLIGHT );
		zsleep(1);
	}

	//This is the "public" method. The one that is actually used by other code to write to the display.
	public void LCDwriteString(String s, int line) {
		switch (line) {
		case 1: LCDwriteCMD(0x80); break;
		case 2: LCDwriteCMD(0xC0); break;
		case 3: LCDwriteCMD(0x94); break;
		case 4: LCDwriteCMD(0xD4); break;
		default: return;	//invalid line number does nothing.
		}

		//limit to 20 chars/line so we don't have to worry about overflow messing up the display
		if (s.length() > 20) s = s.substring(0, 20); 

		for (int i=0; i<s.length(); i++){
			LCDwriteChar(s.charAt(i));
		}
	}

	public void showLoading(){
		String lcdWrite = "";

		LCDwriteString("Running Tests..", 2);

		while(testing){
			for (int i = 0; i < 20; i++) {
		
				//Generating spacing
				for (int j = 0; j < i; j++) {
						lcdWrite += " ";
				}
				lcdWrite += "O";
	
				LCDwriteString(lcdWrite, 1);
				zsleep(500);
				lcdWrite = "";
				clear();
			}
		}

	}

	public void printBlink(String printData, int line, int blinkDelay, int numberOfBlinking)
	{
		for (int i = 0; i < numberOfBlinking; i++) {

			LCDwriteString(printData, line);
			zsleep(blinkDelay);
			clear();
			
		}
	}

	public void clear(){
		this.LCDwriteString("", 1);
		this.LCDwriteString("", 2);
	}

	

    private void testEncoders()
    {
		testing = true;
		driver.resetEncoders();
		flipper.reset();
		elevator.getencoder().reset();
        driver.drive(0.001, 0.001);
		zsleep(1000);
		flipper.rotate(0.001);
		zsleep(800);
		elevator.move(0.01);
        zsleep(800);
        
        if(driver.getLeftEncoder().get() == 0)
        {
            problems.add("Encoder drive left not connected");
        }else if(driver.getRightEncoder().get() == 0)
        {
            problems.add("Encoder drive right not connected");
        }else if(flipper.getEncoder().get() == 0)
        {
            problems.add("Flliper encoder not connected");
        }else if(elevator.getencoder().get() == 0)
        {
            problems.add("Elevator Encoder not connected");
		}
		testing = false;
		loadThread.interrupt();
    }

    public void testBatteryVoltage()
    {
        if(powerDistributionPanel.getVoltage() < 12.5)
        {
            problems.add("Battery LOW");
        }
	}
	
	public void makeAutoTest()
	{
		clear();
		testBatteryVoltage();
		testing = true;
		loadThread = new Thread(new Runnable(){
		
			@Override
			public void run() {
				showLoading();
			}
		});
		testEncoders();
		

		loadThread.start();

	}

	public void printProblems()
	{
		if(problems.size() > 0){
			for (int i = 0; i < problems.size(); i++) {
			
				LCDwriteString(problems.get(i), 1);
				printBlink(problems.get(i),1,1,5);
				clear();
			}
		}else{
			LCDwriteString("No problems were found", 1);
			zsleep(3000);
			clear();
		}

		
	}


    
}

